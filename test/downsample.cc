#include <cairommconfig.h>

#include <cairomm/context.h>
#include <cairomm/surface.h>
#include <iostream>
#include <sstream>
#include <vector>

#include <wavr/waveform/waveform.h>

#include "utils.h"

#ifndef DATA_SIZE
#define DATA_SIZE 44100 * 300 // 5 minutes of data at 44.1 kHz
#endif

using namespace Wavr;

int main() {
    std::vector<float> *data = open_pcm_float("track_tests.pcm");
    Wavr::Waveform wavr(*data);
    dsection_result_t result;
    auto mipmap = wavr.get_mipmap();
    auto sections = wavr.get_all_sections();

    std::cout << "Building waveform from mipmaps" << std::endl;
    draw_waveform("mipmaps.png", mipmap);
    std::cout << "Building wafevorm from sections" << std::endl;
    for (size_t i = 0; i < sections.size(); i++) {
        std::ostringstream filename;
        std::cout << "Section " << i + 1 << ": " << sections[i].size() << " elements" << std::endl;
        filename << "section_" << i << "_" << sections[i].size() << ".png";
        draw_sections(filename.str(), sections[i]);
    }

    return 0;
}
