#pragma once

#include <fstream>
#include <vector>

#include <cairommconfig.h>

#include <cairomm/context.h>
#include <cairomm/surface.h>

#include <wavr/waveform/waveform.h>

#ifndef IMG_WIDTH
#define IMG_WIDTH 1000
#endif

#ifndef IMG_HEIGHT
#define IMG_HEIGHT 200
#endif

#define DATA_SCALE 1

using namespace Cairo;

std::vector<float> *open_pcm_float(std::string filename) {
    std::ifstream file(filename, std::ios::in | std::ios::binary);
    std::vector<float> *data = new std::vector<float>();
    file.seekg(0, std::ios::end);
    const size_t elements = file.tellg() / sizeof(float);
    data->resize(elements);
    file.seekg(0, std::ios::beg);
    file.read(reinterpret_cast<char *>(data->data()), elements * sizeof(float));

    return data;
}

void draw_waveform(std::string filename, std::vector<float> &data) {
    RefPtr<Surface> surface = ImageSurface::create(FORMAT_RGB24, IMG_WIDTH, IMG_HEIGHT);
    RefPtr<Context> ctx = Context::create(surface);

    ctx->set_source_rgb(1.0, 1.0, 1.0);
    ctx->paint();
    ctx->set_source_rgb(0.0, 0.0, 0.0);
    ctx->set_line_width(1.5);
    ctx->move_to(0, IMG_HEIGHT / 2);
    for (unsigned int i = 0; i < data.size(); i++) {
        const double x = (double)i / data.size() * IMG_WIDTH;
        const double y = (data.at(i) * DATA_SCALE + 0.5) * IMG_HEIGHT;
        ctx->line_to(x, y);
    }
    ctx->stroke();

    surface->write_to_png(filename);
}

void draw_sections(std::string filename, std::vector<Wavr::WaveformSection> &extrema) {
    RefPtr<Surface> surface = ImageSurface::create(FORMAT_RGB24, IMG_WIDTH, IMG_HEIGHT);
    RefPtr<Context> ctx = Context::create(surface);

    ctx->set_source_rgb(1.0, 1.0, 1.0);
    ctx->paint();
    ctx->set_source_rgb(0.0, 0.0, 0.0);
    ctx->set_line_width(1.5);
    ctx->move_to(0, IMG_HEIGHT / 2);
    for (unsigned int i = 0; i < extrema.size(); i++) {
        const double x = (double)i / extrema.size() * IMG_WIDTH;
        const double y = (extrema.at(i).max * DATA_SCALE + 0.5) * IMG_HEIGHT;
        ctx->line_to(x, y);
    }
    ctx->stroke();

    ctx->move_to(0, IMG_HEIGHT / 2);
    for (unsigned int i = 0; i < extrema.size(); i++) {
        const double x = (double)i / extrema.size() * IMG_WIDTH;
        const double y = (extrema.at(i).min * DATA_SCALE + 0.5) * IMG_HEIGHT;
        ctx->line_to(x, y);
    }

    surface->write_to_png(filename);
}
