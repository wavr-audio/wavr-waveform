#include <vector>

#include <gtkmm.h>

#include <wavr/waveform/gtk.h>
#include <wavr/waveform/waveform.h>

#include "utils.h"

int main() {
    auto app = Gtk::Application::create("com.gitlab.Wavr.WaveformTest");
    Gtk::Window win;
    auto track = open_pcm_float("track.pcm");
    Wavr::Waveform wform(*track);
    std::vector<Wavr::Waveform> waveforms(1);
    waveforms[0] = wform;

    g_object_set(gtk_settings_get_default(), "gtk-application-prefer-dark-theme", TRUE, NULL);
    Wavr::GtkWaveform wform_wgt(waveforms);
    win.add(wform_wgt);
    wform_wgt.show();

    return app->run(win);
}
