#include <cassert>
#include <chrono>
#include <ctime>
#include <iostream>

#include <wavr/waveform/waveform.h>

#include "utils.h"

#ifndef ITERATIONS
#define ITERATIONS 16
#endif

int main() {
    auto data = open_pcm_float("track.pcm");
    auto start = std::chrono::system_clock::now();
    for (unsigned int i = 0; i < ITERATIONS; i++) {
        Wavr::Waveform wform(*data);
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> duration_total = end - start;
    auto duration_one = duration_total / ITERATIONS;

    std::cout << "Total duration (" << ITERATIONS << " iterations): " << duration_total.count() << std::endl;
    std::cout << "One iteration (avg): " << duration_one.count() << std::endl;
    assert(duration_one.count() < 1.0);
}
