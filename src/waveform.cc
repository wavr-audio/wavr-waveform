#include <wavr/waveform/waveform.h>

using namespace Wavr;

dvec_t do_downsample(const dvec_t &data, int factor) {
    dvec_t result;
    result.reserve(data.size() / factor + 1);
    for (unsigned int i = 0; i < data.size(); i += factor) {
        result.push_back(data.at(i));
    }

    return result;
}

Waveform::Waveform(const dvec_t &data) : m_data(data) {
    build_mipmap();
    build_sections();
}

void Waveform::load(const dvec_t &data) {
    m_data.resize(data.size());
    std::copy(data.begin(), data.end(), m_data.begin());
    build_mipmap();
    build_sections();
}

void Waveform::build_mipmap() { m_mipmap = do_downsample(m_data, WAVEFORM_MIPMAP_DOWNSAMPLE); }

void Waveform::build_sections() {
    size_t steps = std::log(m_mipmap.size()) / std::log(WAVEFORM_SECTIONS_DOWNSAMPLE);
    size_t length = (size_t)WAVEFORM_SECTIONS_DOWNSAMPLE;
    m_sections.resize(steps);
    for (size_t i = 0; i < steps; i++) {
        length = (size_t)std::pow(WAVEFORM_SECTIONS_DOWNSAMPLE, i + 1);
        m_sections.reserve(m_mipmap.size() / length);
        for (unsigned int j = 0; j < m_mipmap.size(); j += length) {
            WaveformSection sec;
            auto begin = m_mipmap.begin() + j;
            auto end = std::min(begin + length, m_mipmap.end());
            std::pair<dvec_t::iterator, dvec_t::iterator> minmax = std::minmax_element(begin, end);
            sec.min = *minmax.first;
            sec.max = *minmax.second;
            m_sections[i].push_back(sec);
        }
    }
}
size_t Waveform::get_length() { return m_data.size(); }

dvec_t &Waveform::get_mipmap() { return m_mipmap; }
void Waveform::get_sections(dsection_result_t *result) {
    result->decimation = get_length() / m_sections[0].size();
    result->section = m_sections[0];
}
void Waveform::get_sections(double density, dsection_result_t *result) {
    size_t i = 0;
    result->decimation = WAVEFORM_SECTIONS_DOWNSAMPLE;
    for (; i < m_sections.size(); i++) {
        result->decimation = get_length() / m_sections[i].size();
        if ((size_t)density <= result->decimation * 2) {
            break;
        }
    }
    result->section = m_sections[i];
}

dvec_t &Waveform::get_data() { return m_data; }
