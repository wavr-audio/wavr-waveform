#include <wavr/waveform/gtk.h>

using namespace Wavr;

GtkWaveform::GtkWaveform(std::vector<Waveform> &src)
    : Glib::ObjectBase("wavr-waveform"), Gtk::DrawingArea(), m_waveforms(src), m_dispatcher(), m_win_start(0),
      m_win_end(0) {
    set_name("wavr-waveform");
    set_has_window();
    add_events(Gdk::EventMask::SCROLL_MASK);

    m_dispatcher.connect(sigc::mem_fun(*this, &GtkWaveform::dispatch_callback));

    m_win_end = get_longest();
}

void GtkWaveform::update(const std::vector<dvec_t> &src) {
    m_waveforms.resize(src.size());
    for (size_t i = 0; i < src.size(); i++) {
        m_waveforms[i].load(src[i]);
    }
    m_dispatcher.emit();
}

Glib::RefPtr<GtkWaveform> GtkWaveform::load(const std::vector<dvec_t> &src) {
    std::vector<Waveform> waveforms(src.size());
    for (size_t i = 0; i < src.size(); i++) {
        waveforms[i].load(src.at(i));
    }

    return Glib::RefPtr<GtkWaveform>(new GtkWaveform(waveforms));
}

bool GtkWaveform::on_draw(const Cairo::RefPtr<Cairo::Context> &ctx) {
    auto alloc = get_allocation();
    size_t size = m_win_end - m_win_start;
    double density = (double)size / (double)alloc.get_width();
    std::cout << "Density: " << density << std::endl;
    auto style_ctx = get_style_context();
    if (size == 0)
        return true;

    ctx->translate(alloc.get_x(), alloc.get_y());
    style_ctx->render_background(ctx, 0, 0, alloc.get_width(), alloc.get_height());
    ctx->set_source_rgb(1.0, 1.0, 1.0);
    ctx->set_line_width(1.0);
    ctx->save();
    if (density < WAVEFORM_MIPMAP_DOWNSAMPLE) {
        draw_data(ctx, density);
    } else if (density < WAVEFORM_MIPMAP_DOWNSAMPLE * WAVEFORM_SECTIONS_DOWNSAMPLE) {
        draw_mipmap(ctx, density);
    } else {
        draw_sections(ctx, density);
    }
    ctx->restore();

    return true;
}

void GtkWaveform::draw_data(const Cairo::RefPtr<Cairo::Context> &ctx, double density) {
    auto alloc = get_allocation();
    double height = alloc.get_height() / (double)m_waveforms.size();
    double x_step = alloc.get_width() / (double)(m_win_end - m_win_start);
    for (size_t i = 0; i < m_waveforms.size(); i++) {
        ctx->save();
        ctx->translate(0, height / 2);
        ctx->move_to(0, 0);
        double x = 0.0;
        auto data = m_waveforms[i].get_data();
        for (size_t j = m_win_start; j < m_win_end; j++) {
            ctx->line_to(x, data[j] * (height / 2.0));
            x += x_step;
        }
        ctx->stroke();
        ctx->restore();
    }
}

void GtkWaveform::draw_mipmap(const Cairo::RefPtr<Cairo::Context> &ctx, double density) {
    auto alloc = get_allocation();
    double height = alloc.get_height() / (double)m_waveforms.size();
    size_t sstep = WAVEFORM_MIPMAP_DOWNSAMPLE;
    size_t begin = m_win_start / sstep;
    size_t end = m_win_end / sstep;
    double x_step = alloc.get_width() / (double)(end - begin);
    for (size_t i = 0; i < m_waveforms.size(); i++) {
        ctx->save();
        ctx->translate(0, height / 2);
        ctx->move_to(0, 0);
        double x = 0.0;
        auto data = m_waveforms[i].get_mipmap();
        begin = std::max((size_t)1, begin + 1) - 1;
        end = std::min(data.size(), end);
        for (size_t j = begin; j < end; j++) {
            ctx->line_to(x, data[j] * (height / 2.0));
            x += x_step;
        }
        ctx->stroke();
        ctx->restore();
    }
}
void GtkWaveform::draw_sections(const Cairo::RefPtr<Cairo::Context> &ctx, double density) {
    auto alloc = get_allocation();
    double height = alloc.get_height() / (double)m_waveforms.size();
    for (size_t i = 0; i < m_waveforms.size(); i++) {
        dsection_result_t sresult;
        m_waveforms[i].get_sections(density, &sresult);
        auto section = sresult.section;
        size_t sstep = sresult.decimation;
        size_t begin = m_win_start / sstep;
        size_t end = m_win_end / sstep;
        double x_step = alloc.get_width() / (double)(end - begin);
        // Ensure we'll loop only over owned memory
        end = std::min(end, sresult.section.size());
        begin = std::max((size_t)1L, begin + 1) - 1;
        ctx->save();
        ctx->translate(0, height / 2);
        ctx->move_to(0, 0);
        double x = 0.0;
        for (size_t j = begin; j < end; j++) {
            ctx->line_to(x, section[j].max * (height / 2));
            x += x_step;
        }
        for (size_t j = (end - 1); j >= begin; j--) {
            ctx->line_to(x, section[j].min * (height / 2));
            x -= x_step;
            if (begin == 0 && j == begin) {
                std::cout << "j==begin" << std::endl;
                break;
            }
        }
        ctx->fill();
        ctx->restore();
    }
}

size_t GtkWaveform::get_longest() {
    size_t max = 0;
    for (size_t i = 0; i < m_waveforms.size(); i++) {
        max = std::max(max, m_waveforms[i].get_length());
    }
    return max;
}
size_t GtkWaveform::get_longest_mipmap() {
    size_t max = 0;
    for (size_t i = 0; i < m_waveforms.size(); i++) {
        max = std::max(max, m_waveforms[i].get_mipmap().size());
    }

    return max;
}
size_t GtkWaveform::get_longest_sections(double density) {
    size_t max = 0;
    for (size_t i = 0; i < m_waveforms.size(); i++) {
        dsection_result_t sresult;
        m_waveforms[i].get_sections(density, &sresult);
        max = std::max(max, sresult.section.size());
    }
    return max;
}

void GtkWaveform::get_preferred_height_vfunc(int &min, int &natural) const {
    min = 30;
    natural = 100;
}
void GtkWaveform::get_preferred_height_for_width_vfunc(int, int &min, int &natural) const {
    get_preferred_height_vfunc(min, natural);
}
void GtkWaveform::get_preferred_width_vfunc(int &min, int &natural) const {
    min = 30;
    natural = 300;
}
void GtkWaveform::get_preferred_width_for_height_vfunc(int, int &min, int &natural) const {
    get_preferred_width_vfunc(min, natural);
}
bool GtkWaveform::on_scroll_event(GdkEventScroll *event) {
    size_t size = m_win_end - m_win_start;
    double x_pos = event->x / get_allocation().get_width();
    if (size < 10 && event->direction == GdkScrollDirection::GDK_SCROLL_UP)
        return false;
    if (event->direction == GdkScrollDirection::GDK_SCROLL_DOWN) {
        size_t size = m_win_end - m_win_start;
        if (m_win_start != 0)
            m_win_start = (size_t)(m_win_start - size * 0.1 * x_pos);
        if (m_win_end < get_longest())
            m_win_end = (size_t)(m_win_end + size * 0.1 * (1.0 - x_pos));
    } else if (event->direction == GdkScrollDirection::GDK_SCROLL_UP) {
        std::cout << "up";
        m_win_start = (size_t)(m_win_start + size * 0.1 * x_pos);
        m_win_end = (size_t)(m_win_end - size * 0.1 * (1.0 - x_pos));
        m_dispatcher.emit();
    }
    // Since values are unsigned, compare them to 1 and then slide the result
    m_win_end = std::min(get_longest(), m_win_end);
    m_win_start = std::max((size_t)1, m_win_start + 1) - 1;
    if (m_win_start > m_win_end)
        m_win_start = 0;
    m_dispatcher.emit();

    return false;
}

void GtkWaveform::dispatch_callback() { queue_draw(); }
