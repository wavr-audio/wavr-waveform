#pragma once
#include <algorithm>
#include <array>
#include <cmath>
#include <vector>

#define WAVEFORM_MIPMAP_DOWNSAMPLE 8
#define WAVEFORM_SECTIONS_DOWNSAMPLE 4

namespace Wavr {
typedef struct {
    float min;
    float max;
} WaveformSection;

typedef std::vector<float> dvec_t;
typedef std::vector<WaveformSection> dsection_t;
typedef struct {
    dsection_t section;
    size_t decimation;
} dsection_result_t;

class Waveform {

public:
    Waveform() = default;
    ~Waveform() = default;
    Waveform(const dvec_t &data);
    void load(const dvec_t &data);
    dvec_t &get_mipmap();
    void get_sections(dsection_result_t *result);
    void get_sections(double density, dsection_result_t *result);
    std::vector<dsection_t> &get_all_sections() { return m_sections; }
    dvec_t &get_data();
    size_t get_length();

private:
    void build_mipmap();
    void build_sections();
    dvec_t m_data;
    dvec_t m_mipmap;
    std::vector<dsection_t> m_sections;
};
} // namespace Wavr
