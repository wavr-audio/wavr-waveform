#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#include <gtkmm.h>

#include "waveform.h"

namespace Wavr {
class GtkWaveform : public Gtk::DrawingArea {
public:
    GtkWaveform(std::vector<Waveform> &m_waveforms);
    void update(const std::vector<dvec_t> &src);
    static Glib::RefPtr<GtkWaveform> load(const std::vector<dvec_t> &src);

protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context> &ctx) override;
    size_t get_longest();
    size_t get_longest_mipmap();
    size_t get_longest_sections(double density);
    void get_preferred_width_vfunc(int &minimum_width, int &natural_width) const override;
    void get_preferred_height_for_width_vfunc(int width, int &minimum_height, int &natural_height) const override;
    void get_preferred_height_vfunc(int &minimum_height, int &natural_height) const override;
    void get_preferred_width_for_height_vfunc(int height, int &minimum_width, int &natural_width) const override;
    bool on_scroll_event(GdkEventScroll *event) override;

private:
    void dispatch_callback();
    void draw_data(const Cairo::RefPtr<Cairo::Context> &ctx, double density);
    void draw_mipmap(const Cairo::RefPtr<Cairo::Context> &ctx, double density);
    void draw_sections(const Cairo::RefPtr<Cairo::Context> &ctx, double density);
    std::vector<Waveform> m_waveforms;
    Glib::Dispatcher m_dispatcher;
    size_t m_win_start;
    size_t m_win_end;
};
} // namespace Wavr
